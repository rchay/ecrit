# 🚀 Algorithmique

## :fontawesome-solid-gears: 2022

- [Métropole, J2, Exercice 1](./Algo/22-ME2-ex1.md)
    - Arbres binaires de recherche

- [Métropole, J1, Exercice 4](./Algo/22-ME1-ex4.md)
    - Somme d'arbres binaires

- [Polynésie, J1, Exercice 5](./Algo/22-PO1-ex5.md)
    - Construction d'arbres binaires

- [Centres étrangers, J2, Exercice 1](./Algo/22-G11-J2-ex1.md)
    - Produit d'une chaine de caractères et d'un entier

## :fontawesome-solid-gears: 2021

- [Métropole, J2, Exercice 3](./Algo/21-ME2-ex3.md)
    - Arbres binaires de recherche


