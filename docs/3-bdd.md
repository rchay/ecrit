# 💾 Bases de données, SQL

## :fontawesome-solid-database: 2023

- [Agro Veto](./BDD/23-agro-veto.md)

## :fontawesome-solid-database: 2022

- [Métropole, J1, Exercice 2](./BDD/22-ME1-ex2.md)
    - 3 relations dans une base de données sur le cinéma
    - 2 tables : **`individu`** et **`realisation`**

- [Métropole, J2, Exercice 4](./BDD/22-ME2-ex4.md)
    - 2 relations dans une base de données sur la musique
    - 2 tables : **`morceaux`** et **`interpretes`**

- [Polynésie, J1, Exercice 3](./BDD/22-PO1-ex3.md)
    - Données de navigation recueillies dans **`Visites`**
        - `(identifiant, adresse IP, date et heure de visite, nom de la page, navigateur)`
    - Une table **`Pings`** avec les durées de connexion

- [Centres étrangers, J2, Exercice 3](./BDD/22-G11-J2-ex3.md)
    - Évaluations d'élèves par compétence
    - Une table **`Evaluations`** et une table **`Resultats`**

- [Centres étrangers, J1, Exercice 4](./BDD/22-G11-J1-ex4.md)
    - Mesures du réchauffement climatique
    - Une table **`Centres`** et une table **`Mesures`**


## :fontawesome-solid-database: 2021

- [Métropole, Candidats libres, J2, Exercice 1](./BDD/21-ME2-ex1.md)
    - 2 relations dans une base de données sur un CDI
    - 3 tables : **`Livres`**, **`Emprunts`** et **`Eleves`**


## :fontawesome-solid-database: 2018

- [Centrale 2018](./BDD/18-centrale.md)

## :fontawesome-solid-database: 2016

- [Centrale 2016](./BDD/16-centrale.md)
