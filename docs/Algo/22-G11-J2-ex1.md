---
title: Produit de chaines de caractères par un entier
author: Franck Chambon
---

> D'après 2022, Centres étrangers, J2, Ex. 1

**1.** Voici une fonction codée en Python :

```python
def f(n):
    if n == 0:
        print("Partez !")
    else:
        print(n)
        f(n-1)
```

**1.a.** Qu'affiche la commande `f(5)` ?

??? done "Réponse"
    `f(5)` affiche

    ```
    5
    4
    3
    2
    1
    Partez !
    ```

**1.b.** Pourquoi dit-on de cette fonction qu'elle est récursive ?

??? done "Réponse"
    Le code source de `f` contient un appel à elle-même, c'est donc une fonction récursive.




**2.** On rappelle qu'en python l'opérateur `+` a le comportement suivant sur les chaines de caractères :

```pycon
>>> S = 'a' + 'bc'
>>> S
'abc'
```

Et le comportement suivant sur les listes :

```pycon
>>> L = ['a'] + ['b', 'c']
>>> L
['a', 'b', 'c']
```

On a besoin pour les questions suivantes de pouvoir ajouter une chaine de caractères `s` en préfixe à chaque chaine de caractères de la liste `chaines`.

On appellera cette fonction `ajouter`.

Par exemple, `#!py ajouter("a", ["b", "c"])` doit renvoyer `["ab", "ac"]`.

**2.a.** Recopiez le code suivant et complétez `...` sur votre copie :

```python
def ajouter(s, chaines):
    resultat = []
    for mot in chaines:
        resultat ...
    return resultat
```

??? done "Réponses"

    ```python
    def ajouter(s, chaines):
        resultat = []
        for mot in chaines:
            resultat.append(s + mot)
        return resultat
    ```

**2.b.** Que renvoie la commande `#!py ajouter("b", ["a", "b", "c"])` ?

??? done "Réponse"
    `#!py ajouter("b", ["a", "b", "c"])` renvoie `["ba", "bb", "bc"]`.

**2.c.** Que renvoie la commande `#!py ajouter("a", [""])` ?

??? done "Réponse"
    `#!py ajouter("a", [""])` renvoie `["a"]`.

**3.** On s'intéresse ici à la fonction suivante écrite en Python où `s` est une chaine de caractères et `n` un entier naturel.

```python
def produit(s, n):
    if n == 0:
        return [""]
    else:
        resultat = []
        for c in s:
            resultat = resultat + ajouter(c, produit(s, n - 1))
        return resultat
```

**3.a.** Que renvoie la commande `#!py produit("ab", 0)` ? Le résultat est-il une liste vide ?

??? done "Réponse"
    `#!py produit("ab", 0)` utilise le paramètre `n` égal à `0`, donc elle renvoie `#!py [""]`. (L'affichage en console sera `#!py ['']`)

    `#!py [""]` n'est pas une liste vide, c'est une liste qui contient **un** élément : la chaine de caractères vide.

**3.b.** Que renvoie la commande `#!py produit("ab", 1)` ?

??? done "Réponse"
    `#!py produit("ab", 1)` fait une boucle `#!py for` avec deux tours :

    - Premier tour, avec `c = 'a'`, `resultat` devient `["a"]`.
    - Second tour, avec `c = 'b'`,  `resultat` devient `["a"] + ["b"]`.

    `#!py ['a', 'b']` est renvoyé.

**3.c.** Que renvoie la commande `#!py produit("ab", 2)` ?

??? done "Réponse"
    `#!py produit("ab", 2)` fait une boucle `#!py for` avec deux tours :

    - Premier tour, avec `c = 'a'`, `resultat` devient `["aa", "ab"]`.
    - Second tour, avec `c = 'b'`,  `resultat` devient `["aa", "ab"] + ["ba", "bb"]`.

    `#!py ['aa', 'ab', 'ba', 'bb']` est renvoyé.

